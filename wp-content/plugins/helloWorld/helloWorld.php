<?php
/**
 * @package Hello_World
 * @version 1.0.0
 */
/*
Plugin Name: Hello World
Plugin URI: http://testhw.com
Description: test exemple plugin
Author: Maeva
Version: 1.0.0
Author URI: http://testhw.com
*/

add_action('wp_footer', 'dit_salut');
add_filter('default_content', 'exemple_de_contenu');
add_filter('the_content', 'a_mettre_apres_contenu');
add_shortcode('nouveauShortCodeCree', 'creation_short_code');


function dit_salut(){
    echo('<p>Bonjour à tous. Pourquoi omettre de nous signifier clairement que Wordpress peut être cool ?</p>');
}
function exemple_de_contenu(){
    return 'Template par défaut :
    
    Titre
    
    Titre 2
    
    Contenu';
}
function a_mettre_apres_contenu($content){
    return $content.'<p>Hello World</p>';
}
// function creation_short_code(){
//     echo('<p>coucou je suis un shortcode<p>')
// }