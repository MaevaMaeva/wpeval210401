<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wpeval210401' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'mysql' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'mysql' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.;<%c:G:rBakNPU]djN`g|]+,ZDr61dC_aU1Ie;8wZzMm=K**N7=T.:vhA=q~^`E' );
define( 'SECURE_AUTH_KEY',  'vl:PpT!Lk=)m>J+l=^-:|p;>S9BwYFO{_IvKq{9v)6V!js5[3,NoFu{d-KZ#$34-' );
define( 'LOGGED_IN_KEY',    '1E{O}ya.SJ!:r]#}L?9h.xh2-jf590B5xz%D3Wa,=VjMif@t}_p}R=(7R{e,F^ <' );
define( 'NONCE_KEY',        '0Nej7Dn]+e&+%Y6PBT 8}E``z2{#LtHWp)0I6y1L%I7xW_j:4XiGFy4n7s}u/{9#' );
define( 'AUTH_SALT',        'S4/Y*f3:RQ$@h`8PHHapPqnxX|ojw>1dNe$Mj526fpA`4gXjXv.l?m)Z3mC$P[KK' );
define( 'SECURE_AUTH_SALT', 'M?(RnZSBHJDSiJ@T(X^jMA&.V93_d-5Ra*BEZ+?4Sf8sOoW+q]FBh+4ew=)Thwm#' );
define( 'LOGGED_IN_SALT',   'kX:M3!IB?U/X+Jm]4A9CGo:i?7Y~f?d{fE_m.W8 jxzre+ECL,)7gl/P/vn^^J!G' );
define( 'NONCE_SALT',       'm(0+#3A]Lf>}J2>1WIBGzw+6Y9MmXB;Qe2mq[[Z5AYQ|;xti3DrZ2{vDGs+>-ul/' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
