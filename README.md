Contexte du projet

On vous demande de mettre en oeuvre un site basé sur Wordpress, sur lequel vous allez développer une fonctionnalité spécifique, sous forme de plugin. Le menu devra permettre de naviguer sur l'accueil, l'annuaire, ainsi qu'une page d'information sur l'identité de l'auteur du site. La charte graphique est "gris anthracite", pas de logo. La page annuaire présente une liste des entreprises que vous avez croisées dans votre parcours professionnel, dans les domaines de la tech et du numérique, et dont les données sont stockées dans une table MySQL (script fourni).

    **Bonus backend **: ajoutez une interface graphique dans le backoffice Wordpress, qui permette d'éditer les entreprises.
    Bonus frontend : prévoyez une pagination, ainsi que des fonctionnalités de recherche et de tri.
    Précision : l'utilisation d'un plugin déjà existant est (évidemment) interdit !

Modalités pédagogiques

Vous avez deux jours. Travail individuel, à réaliser en autonomie.
Modalités d'évaluation

    Le site est fonctionnel, les éléments demandés sont présents, la charte graphique est respectée.
    L'annuaire affiche le contenu de la table wp_annuaire avec succès.

Livrables

L'évaluation sera réalisée sur la base d'une **présentation live** à l'un de vos formateurs, ainsi que la fourniture de votre **dépôt GIT**, contenant le **code source** et le **script SQL**. La deadline doit être respectée.


# configurer la bdd
https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/file/txt/81e721b3-9b8f-4f70-987b-bea83848417b.txt

cloner le projet

lancer apache